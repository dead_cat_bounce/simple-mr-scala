# simple-mr-scala
A simple, manual implimentation of mapreduce in scala as an exercise.  The "reducer" will accept a string of input and count the number of occurances of each word contained in the string.  A subset of punctuation ("?",".",",","!","&") will be stripped from words and considered independent words.

# Prerequisites
* Java 1.7 or greater
* scala 2.11.x
* SBT 0.13.6 or greater

# Build
1. Clone `simple-mr-scala` and cd into the `simple-mr-scala` directory.
2. Execute the following command:
```
human@machine:~/simple-mr-scala$ sbt assembly
```

If all has gone to plan you should have a new file named `reduce-assembly-0.0.1.jar` located at `simple-mr-scala/target/scala-2.11/reduce-assembly-0.0.1.jar`.

# Execute
1. Navigate to the directory containg `reduce-assembly-0.0.1.jar`.
2. Execute the following command:
```
human@machine:~/simple-mr-scala/target/scala-2.11$ java -jar "my string input..."
```

# Example
If we run the above command with the following input:
> "The Pequod sails northeast toward Formosa and into the Pacific Ocean. Ahab with one nostril smells the musk from the Bashee isles and with the other the salt of the waters where Moby Dick swims. Ahab goes to Perth, the blacksmith, with bag of race-horse shoe-nail stubs to be forged into the shank of a special harpoon, and with his razors for Perth to melt and fashion into a harpoon barb. Ahab tempers the barb in blood from Queequeg, Tashtego and Daggoo."

The following output can be expected:

```
bkarels@ubuntu:~/exercise/reduce/target/scala-2.11$ java -jar reduce-assembly-0.0.1.jar "The Pequod sails northeast toward Formosa and into the Pacific Ocean. Ahab with one nostril smells the musk from the Bashee isles and with the other the salt of the waters where Moby Dick swims. Ahab goes to Perth, the blacksmith, with bag of race-horse shoe-nail stubs to be forged into the shank of a special harpoon, and with his razors for Perth to melt and fashion into a harpoon barb. Ahab tempers the barb in blood from Queequeg, Tashtego and Daggoo."
Picked up _JAVA_OPTIONS: -Xms512m -Xmx2G -XX:PermSize=256m -XX:MaxPermSize=1G
(fashion,1)
(for,1)
(Formosa,1)
(in,1)
(barb,2)
(shoe-nail,1)
(his,1)
(harpoon,2)
(bag,1)
(razors,1)
(.,4)
(Moby,1)
(Ocean,1)
(Daggoo,1)
(northeast,1)
(tempers,1)
(a,2)
(Bashee,1)
(musk,1)
(to,3)
(toward,1)
(,,4)
(shank,1)
(isles,1)
(The,1)
(swims,1)
(Dick,1)
(sails,1)
(Queequeg,1)
(with,4)
(from,2)
(Ahab,3)
(melt,1)
(forged,1)
(be,1)
(into,3)
(goes,1)
(where,1)
(waters,1)
(salt,1)
(Pacific,1)
(special,1)
(Pequod,1)
(Perth,2)
(race-horse,1)
(Tashtego,1)
(stubs,1)
(smells,1)
(blacksmith,1)
(other,1)
(of,3)
(and,5)
(one,1)
(nostril,1)
(the,9)
(blood,1)
bkarels@ubuntu:~/exercise/reduce/target/scala-2.11$
```
