import com.bradkarels.mr._
import org.specs2.mutable._

class ReduceSpec extends Specification { 

  //val reduce = Reduce

  val tabbedInput = "a	b	c	d	e"
  val spacedInput = "a b  c    d      e"
  val punctOnlyInput = "!,.&!!!"

  val atoe = List[String]("a","b","c","d","e")
  val puncs = List[String]("!",",",".","&","!","!","!")

  "Reduce prepare" should {
    "tokenize a string on tabbed whitespace" in {
      Reduce.prepare(tabbedInput) must be equalTo atoe
    }
    "tokenize a string on variable spaces" in {
      Reduce.prepare(spacedInput) must be equalTo atoe
    }
    "separate a subset of punctuation into individual strings in a list" in {
      Reduce.prepare(punctOnlyInput) must be equalTo puncs
    }
  }

  "Reduce filterPunc" should {
    "strip punctuation from words and return a list with word + punctuation as unique elements" in {
      Reduce.filterPunc("WTF!") must contain("!") and contain("WTF")
      Reduce.filterPunc("then,") must contain(",") and contain("then")
      Reduce.filterPunc("foo.") must contain(".") and contain("foo")
      Reduce.filterPunc("bar&") must contain("&") and contain("bar")
      Reduce.filterPunc(",hey!") must contain(",") and contain("!") and contain("hey")
    }
    "ignore hyphens" in {
      Reduce.filterPunc("shoe-nail") must be equalTo List("shoe-nail")
    }
  }

  "Reduce map" should {
    "return a list of tuple2's from a list of String" in {
      val strings = List("a","b","c")
      val expected = List(("a",1),("b",1),("c",1))
      Reduce.map(strings) must be equalTo expected
    }
  }

  "Reduce reduce" should {
    "do MR on List[(String,Int)]" in {
      val input = List(("&",1),("a",1),("b",1),("c",1),("b",1),("a",1),("a",1))
      val result = Reduce.reduce(input)
      result("a") must be equalTo 3
      result("b") must be equalTo 2
      result("c") must be equalTo 1
      result("&") must be equalTo 1
    }
  }

}
