package com.bradkarels.mr

import annotation.tailrec
import util.matching.Regex

object Reduce {
  def prepare(str:String):List[String] = {
    // Tokenize string on all whitespace (e.g. \t ' ' )
    val raw:List[String] = str.split("\\s+").toList
    raw.flatMap { filterPunc(_) }
  }

  // Rudimentary filter to separate out '?', '!', '.', ',', and '&' as independent string elements.
  def filterPunc(str:String):List[String] = {
    if (str.length > 0) {
      val head = str.head
      val pattern = new Regex("[?!.,&]")
      val punctuation:List[String] = pattern.findAllIn(str).toList
      val words:List[String] = pattern.split(str).toList.filterNot( e => e.isEmpty)
      punctuation ::: words
    } else
      List[String]()
  }
  
  def map(input:List[String]):List[(String,Int)] = {
    input.map { (_, 1) }
  }
  
  @tailrec def reduce(input:List[(String, Int)], result:Map[String, Int] = Map.empty): Map[String,Int] = {
    if (input.size > 0) {
      val head:(String, Int) = input.head
      val tail:Option[List[(String, Int)]] = Some(input.tail)
      
      val newResult = result + (head._1 -> (result.getOrElse(head._1, 0) + 1))
      reduce(tail.getOrElse(List[(String, Int)]()), newResult)
    } else 
      result
  }

  def main(args: Array[String]) {
    val input = if (args.size > 0) args(0) else ""
    if (input.length == 0) {
      println("Nothing to see here.  Give me a string to see something more interesting...")
      System.exit(0)
    }

    val reduction = reduce(map(prepare(input)))

    reduction.foreach(println)
  }
}
/* Sample input of psuedo interesting values...
"The Pequod sails northeast toward Formosa and into the Pacific Ocean. Ahab with one nostril smells the musk from the Bashee isles and with the other the salt of the waters where Moby Dick swims. Ahab goes to Perth, the blacksmith, with bag of race-horse shoe-nail stubs to be forged into the shank of a special harpoon, and with his razors for Perth to melt and fashion into a harpoon barb. Ahab tempers the barb in blood from Queequeg, Tashtego and Daggoo."
*/
