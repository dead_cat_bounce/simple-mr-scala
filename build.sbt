lazy val root = (project in file(".")).
  settings(
    name := "reduce",
    version := "0.0.1",
    scalaVersion := "2.11.7"
)

libraryDependencies ++= Seq("org.specs2" %% "specs2-core" % "3.6.4" % "test")

scalacOptions in Test ++= Seq("-Yrangepos")
